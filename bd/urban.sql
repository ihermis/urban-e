-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for urban
CREATE DATABASE IF NOT EXISTS `urban` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `urban`;

-- Dumping structure for table urban.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `grupo_cliente_id` int(11) NOT NULL,
  `observacion` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table urban.cliente: ~4 rows (approximately)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`id`, `nombre`, `apellido`, `email`, `grupo_cliente_id`, `observacion`) VALUES
	(3, 'hermis e', 'teran 12', 'hermi2s@gmail.com', 2, 'apaaaa'),
	(4, 'hermis', 'teran', 'hermi22s@gmail.com', 3, 'yeeee'),
	(5, 'hermis 3333', 'teran', 'hermis3@gmail.com', 2, ''),
	(6, 'hermis ant', 'teran asas', 'hermisasasasas@gmail.com', 1, 'asasasasas');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Dumping structure for table urban.grupo_cliente
CREATE TABLE IF NOT EXISTS `grupo_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table urban.grupo_cliente: ~4 rows (approximately)
/*!40000 ALTER TABLE `grupo_cliente` DISABLE KEYS */;
INSERT INTO `grupo_cliente` (`id`, `nombre`) VALUES
	(1, 'Grupo 1'),
	(2, 'Grupo 2'),
	(3, 'Grupo 3'),
	(5, 'Grupo 4');
/*!40000 ALTER TABLE `grupo_cliente` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
