<?php 
    include_once('config/constantes.php');
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
    else  
         $url = "http://";   

    $url.= $_SERVER['HTTP_HOST'];   

  ?>   
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Urbano Express</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand" href="<?php echo $url; ?>/urban/"><?php echo NAME_APP; ?></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="<?php echo $url.DIR_ROOT; ?>/views/cliente_view.php">Clientes</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="<?php echo $url.DIR_ROOT; ; ?>/views/grupo_clientes_view.php">Grupo de clientes</a>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>