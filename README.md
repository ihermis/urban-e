# Urban Express - Prueba Hermis

## Preparación de la base de datos
Se debe mofificar los valores de configuración para realizar la conexión
El archivo se encuentra en:
```sh
config/conexion_db.php
```

## Ajustes en los valores de las contantes
* Si es necesario, modificar el nombre del directorio, para correcto funcionamiento de los enlaces de la barra navegadora.
El archivo se encuentra en:
```sh
config/constantes.php
```

## Dump sql
Tmabién adjunto un dump sql con la estructura de la base de datos. El archivo se ubica en:
```sh
bd/urban.sql
```