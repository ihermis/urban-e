<?php
class Conectar
{
    private $host, $user, $pass, $database, $charset;

    public function __construct()
    {
        $this->host = "localhost";
        $this->user = "root";
        $this->pass = "";
        $this->database = "urban";
        $this->charset = "utf8";
    }

    public function conexion()
    {

        $con = new mysqli($this->host, $this->user, $this->pass, $this->database);
        $con->query("SET NAMES '" . $this->charset . "'");

        return $con;
    }
}
