<?php
include_once('../head.php');
?>
<header class="text-center">
    <h1>Gestión de Clientes</h1>
    <hr>

    <div id="msj_alerta"></div>
</header>
<div id="html_view"></div>
            
<div class="row">
    <div class="col-lg-5">
        <h3>Nuevo Cliente</h3>
        <hr/>
        <form id="formCliente">
            <input type="hidden" class="form-control" id="id_cliente" placeholder="" autocomplete="off" value="" >
            <div class="mb-3">
                <label for="nombre" class="form-label">Nombres</label>
                <input type="text" class="form-control" id="nombre" placeholder="" autocomplete="off" value="" maxlength="100">
            </div>
            <div class="mb-3">
                <label for="apellido" class="form-label">Apellidos</label>
                <input type="text" class="form-control" id="apellido" placeholder="" value="" maxlength="100">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input type="email" class="form-control" id="email" placeholder="" value="" maxlength="50">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Grupo de cliente</label>
                <select class="form-control listado_grupo_clientes" id="grupo_cliente"></select>
            </div>
            <div class="mb-3">
                <label for="observacion" class="form-label">Observación</label>
                <textarea class="form-control" id="observacion" rows="3"></textarea>
            </div>
        </form>
        <div class="mb-3 text-center">
            <button type="button" class="btn btn-secondary" id="cancelar_form" onclick="cancelarForm()">Cancelar</button>
            <button type="button" class="btn btn-success" id="guardar_form" onclick="guardarForm()">Guardar</button>
            <button type="button" class="btn btn-warning" id="editar_form" onclick="guardarForm(true)" style="display:none;">Editar</button>
        </div>
    </div>

    <div class="col-lg-7 text-center">
        <h3>Listado de Clientes</h3>
        <hr/>
        <div class="row">
            <div class="col-md-5"><input type="text" class="form-control" id="buscar_text" placeholder="Buscar..." maxlength="100"></div>
            <div class="col-md-4"><select class="form-control listado_grupo_clientes" id="grupo_cliente_buscar"></select></div>
            <div class="col-md-3"><button type="button" class="btn btn-success" id="buscar_clientes">Buscar</button></select></div>
        </div>
        <hr/>
        <div id="listado_clientes"></div>
    </div>
</div>
<?php
include_once('../pre-footer.php');
?>
<script>
        var grupo_clientes = [];
        $(document).ready(function(){
            getGrupoClientes();
            getClientes();

            $('#grupo_cliente_buscar').change(function(){
                getClientes(true);
            });

            $('#buscar_text').keyup(function(e){
                if(e.keyCode == 13) {
                    getClientes(true);
                }
            });

            $('#buscar_clientes').click(function(){
                getClientes(true);
            });
        });


        function getClientes(filtrar = false){
            var action = 'getClientes';
            if(filtrar){
                $('#buscar_clientes').text('Buscando...');
                action = 'buscarClientes';
            }

            var html_table = '';
            $.ajax({
                type: "POST",
                url: "../controllers/clientes.php?action=" + action,
                contentType: "application/x-www-form-urlencoded",
                data:{
                    text_buscar :  $('#buscar_text').val(),
                    grupo_cliente_buscar :  $('#grupo_cliente_buscar').val(),
                    buscar : filtrar
                },
                dataType: "json",
                success: function (res) {
                    if(filtrar){
                        $('#buscar_clientes').text('Buscar');
                    }
                    html_table += '<table class="table table-hover">';
                    html_table += '<thead>';
                    html_table += '<tr>';
                        html_table += '<th scope="col">#</th>';
                        html_table += '<th scope="col">Nombre</th>';
                        html_table += '<th scope="col">Apellido</th>';
                        html_table += '<th scope="col">Email</th>';
                        html_table += '<th scope="col">Grupo</th>';
                        html_table += '<th scope="col"></th>';
                    html_table += '</tr>';
                    html_table += '</thead>';
                    html_table += '<tbody>';

                    //console.log(res);
                    if(res.length){
                        for (let i = 0; i < res.length; i++) {
                            const cliente = res[i];
                            html_table += '<tr>';
                            html_table += '<td>' + cliente.id_cliente +'</td>';
                            html_table += '<td>' + cliente.nombre +'</td>';
                            html_table += '<td>' + cliente.apellido +'</td>';
                            html_table += '<td>' + cliente.email +'</td>';
                            html_table += '<td>' + grupoClientesListado(cliente.id_cliente , cliente.grupo_cliente_id  , cliente.grupo_cliente) +'</td>';
                            html_table += '<th scope="col"><button type="button" class="btn btn-warning" onclick="getClienteId(' + cliente.id_cliente +')"><i class="far fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteCliente(' + cliente.id_cliente +')"><i class="far fa-trash-alt"></i></button></th>';
                            html_table += '</tr>';
                        }
                    }else{
                        html_table += '<tr>';
                        html_table += '<th colspan="6">No hay resultados</th>';
                        html_table += '</tr>';
                    }

                    html_table += '</tbody>';

                    html_table += '</table>';

                    $('#listado_clientes').html(html_table);

                }, error: function (xhr, textStatus, errorThrown) {
                    if(filtrar){
                        $('#buscar_clientes').text('Buscar');
                    }
                        console.log("cx ajax post error:" + xhr.statusText);
                    }
            });
        }

        function guardarForm(update = false){
            var action = 'saveCliente';
            if(update){
                action = 'updateCliente';
            }

            if(validaForm()){

                if(update)
                    $("#editar_form").text('Editando...');
                else
                    $("#guardar_form").text('Guardando...');

                setTimeout(() => {
                    $.ajax({
                    type: "POST",
                    url: "../controllers/clientes.php?action=" + action,
                    data: {
                        id_cliente: $("#id_cliente").val(),
                        nombre: $("#nombre").val(),
                        apellido: $("#apellido").val(),
                        email: $("#email").val(),
                        grupo_cliente: $("#grupo_cliente").val(),
                        observacion: $("#observacion").val(),

                    } ,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (res) {
                        if(update)
                            $("#editar_form").text('Editar');
                        else
                            $("#guardar_form").text('Guardar');

                        if(res.status){
                            $("#guardar_form").css('display', 'initial');
                            $("#editar_form").css('display', 'none');
                            msjTemporal(res.msj , 'success');
                            cancelarForm();
                        }else{
                            msjTemporal(res.msj , 'danger');
                        }

                    }, error: function (xhr, textStatus, errorThrown) {
                        $("#guardar_form").text('Guardar');
                        $("#editar_form").text('Editar');
                            console.log("cx ajax post error:" + xhr.statusText);
                        }
                    });
                }, 1500);
            }
        }

        function deleteCliente(id_cliente){


            if(confirm('Se procederá a eliminar el cliente, hacé click en Ok para continuar.')){

                $.ajax({
                type: "POST",
                url: "../controllers/clientes.php?action=deleteCliente",
                data: {
                    id_cliente: id_cliente
                } ,
                contentType: "application/x-www-form-urlencoded",
                dataType: "json",
                success: function (res) {

                    if(res.status){
                        msjTemporal(res.msj , 'success');
                        getClientes();
                    }else{
                        msjTemporal(res.msj , 'danger');
                    }

                }, error: function (xhr, textStatus, errorThrown) {

                        console.log("cx ajax post error:" + xhr.statusText);
                    }
                });

            }
        }

        function getClienteId(id_cliente){

            $.ajax({
            type: "POST",
            url: "../controllers/clientes.php?action=getClientePorId",
            data: {
                id_cliente: id_cliente,
            } ,
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            success: function (res) {
                if(res.status){
                    $("#guardar_form").css('display', 'none');
                    $("#editar_form").css('display', 'initial');
                    $("#id_cliente").val(res.data.id_cliente);
                    $("#nombre").val(res.data.nombre);
                    $("#apellido").val(res.data.apellido);
                    $("#email").val(res.data.email);
                    $("#grupo_cliente").val(res.data.grupo_cliente_id);
                    $("#observacion").val(res.data.observacion);
                }else{
                    msjTemporal(res.msj , 'danger');
                }

            }, error: function (xhr, textStatus, errorThrown) {
                $("#guardar_form").text('Guardar');
                    console.log("cx ajax post error:" + xhr.statusText);
                }
            });

        }

        function validaForm(){

            if($("#nombre").val() == ""){
                msjTemporal("Ingresá el nombre del cliente." , 'danger');
                $("#nombre").focus();
                return false;
            }
            if($("#apellido").val() == ""){
                msjTemporal("Ingresá el apellido del cliente." , 'danger');
                $("#apellido").focus();
                return false;
            }
            if($("#grupo_cliente").val() == ""){
                msjTemporal("Selecciona un grupo de usuario" , 'danger');
                $("#grupo_cliente").focus();
                return false;
            }
            if($("#email").val().indexOf('@', 0) == -1 || $("#email").val().indexOf('.', 0) == -1) {
                msjTemporal("El email introducido no es correcto." , 'danger');
                return false;
            }

            if($("#observacion").val() == '') {
                msjTemporal("Ingresá una observación." , 'danger');
                return false;
            }

            return true;
        }

        function msjTemporal(msj , clase){
            html = '<div class="alert alert-'+clase+'" role="alert">';
            html += msj;
            html += '</div>';

            $('#msj_alerta').html(html);
            setTimeout(() => {
                $('#msj_alerta').empty();
            }, 3000);
        }

        function cancelarForm(){
            $("#nombre").val('');
            $("#apellido").val('');
            $("#email").val('');
            $("#grupo_cliente").val('');
            $("#observacion").val('');
            getClientes();
        }

        function getGrupoClientes(){
            var html_select = '';
            $.ajax({
                type: "POST",
                url: "../controllers/clientes.php?action=getGrupoClientes",
                contentType: "application/x-www-form-urlencoded",
                dataType: "json",
                success: function (res) {
                    if(res.length){
                        html_select += '<option value="">Grupos de cliente</option>';

                        grupo_clientes = res;
                        for (let i = 0; i < res.length; i++) {
                            const cliente = res[i];
                            html_select += '<option value="' + cliente.id_grupo_cliente +'">' + cliente.nombre +'</option>';
                        }

                        $('.listado_grupo_clientes').html(html_select);
                    }

                }, error: function (xhr, textStatus, errorThrown) {
                        console.log("cx ajax post error:" + xhr.statusText);
                    }
            });
        }

        function grupoClientesListado(id_cliente , id_grupo_cliente , nombre_grupo_cliente){

            var html = '<div class="btn-group">';
                html += '<button type="button" class="btn btn-secondary" id="gc_' +id_cliente +'">'+ nombre_grupo_cliente +'</button>';
                html += '<button type="button" class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">';
                html += '    <span class="visually-hidden">Toggle Dropdown</span>';
                html += '</button>';
                html += '<ul class="dropdown-menu">';
                for (let i = 0; i < grupo_clientes.length; i++) {
                    const cliente = grupo_clientes[i];
                    if(cliente.id_grupo_cliente != id_grupo_cliente)
                        html += '<li><a class="dropdown-item" href="javascript:cambiarGrupoCliente(' +id_cliente +' ,' + cliente.id_grupo_cliente +' , `' + cliente.nombre +'`)">' + cliente.nombre +'</a></li>';
                }
                html += '</ul>';
                html += '</div>';

                return html;


        }

        function cambiarGrupoCliente(id_cliente , id_grupo_cliente , nombre_grupo_cliente){
            if(confirm('Se modificará el grupo de cliente actual, hacé click en Ok para continuar.')){

                $.ajax({
                type: "POST",
                url: "../controllers/clientes.php?action=cambiarGrupoCliente",
                data: {
                    id_cliente: id_cliente ,
                    id_grupo_cliente , id_grupo_cliente
                } ,
                contentType: "application/x-www-form-urlencoded",
                dataType: "json",
                success: function (res) {

                    if(res.status){
                        msjTemporal(res.msj , 'success');
                        $('#gc_' + id_cliente).text(nombre_grupo_cliente);
                    }else{
                        msjTemporal(res.msj , 'danger');
                    }

                }, error: function (xhr, textStatus, errorThrown) {

                        console.log("cx ajax post error:" + xhr.statusText);
                    }
                });

            }
        }
    </script>

<?php
include_once('../footer.php');
?>