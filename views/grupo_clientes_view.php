<?php
include_once('../head.php');
?>
<header class="text-center">
    <h1>Gestión de Grupo de Clientes</h1>
    <hr>

    <div id="msj_alerta"></div>
</header>
<div id="html_view"></div>
            
<div class="row">
    <div class="col-lg-5">
        <h3>Nuevo Grupo de Clientes</h3>
        <hr/>
        <form id="formCliente">
            <input type="hidden" class="form-control" id="id_grupo_cliente" placeholder="" autocomplete="off" value="" >
            <div class="mb-3">
                <label for="nombre" class="form-label">Nombres</label>
                <input type="text" class="form-control" id="nombre" placeholder="" autocomplete="off" value="" maxlength="100">
            </div>
        </form>
        <div class="mb-3 text-center">
            <button type="button" class="btn btn-secondary" id="cancelar_form" onclick="cancelarForm()">Cancelar</button>
            <button type="button" class="btn btn-success" id="guardar_form" onclick="guardarForm()">Guardar</button>
            <button type="button" class="btn btn-warning" id="editar_form" onclick="guardarForm(true)" style="display:none;">Editar</button>
        </div>
    </div>

    <div class="col-lg-7 text-center">
        <h3>Listado de Grupo de Clientes</h3>
        <hr/>
        <div class="row">
            <div class="col-md-8"><input type="text" class="form-control" id="buscar_text" placeholder="Buscar..." maxlength="100"></div>
            <div class="col-md-4"><button type="button" class="btn btn-success" id="buscar_clientes">Buscar</button></select></div>
        </div>
        <hr/>
        <div id="listado_clientes"></div>
    </div>
</div>
<?php
include_once('../pre-footer.php');
?>
<script>
        var grupo_clientes = [];
        $(document).ready(function(){
            getGrupoClientes();

            $('#grupo_cliente_buscar').change(function(){
                getGrupoClientes(true);
            });

            $('#buscar_text').keyup(function(e){
                if(e.keyCode == 13) {
                    getGrupoClientes(true);
                }
            });

            $('#buscar_clientes').click(function(){
                getGrupoClientes(true);
            });
        });


        function getGrupoClientes(filtrar = false){
            var action = 'getGrupos';
            if(filtrar){
                $('#buscar_clientes').text('Buscando...');
                action = 'buscarGrupo';
            }

            var html_table = '';
            $.ajax({
                type: "POST",
                url: "../controllers/grupo_clientes.php?action=" + action,
                contentType: "application/x-www-form-urlencoded",
                data:{
                    text_buscar :  $('#buscar_text').val(),
                    grupo_cliente_buscar :  $('#grupo_cliente_buscar').val(),
                    buscar : filtrar
                },
                dataType: "json",
                success: function (res) {
                    if(filtrar){
                        $('#buscar_clientes').text('Buscar');
                    }
                    html_table += '<table class="table table-hover">';
                    html_table += '<thead>';
                    html_table += '<tr>';
                        html_table += '<th scope="col">#</th>';
                        html_table += '<th scope="col">Nombre</th>';
                        html_table += '<th scope="col"></th>';
                    html_table += '</tr>';
                    html_table += '</thead>';
                    html_table += '<tbody>';

                    //console.log(res);
                    if(res.length){
                        for (let i = 0; i < res.length; i++) {
                            const cliente = res[i];
                            html_table += '<tr>';
                            html_table += '<td>' + cliente.id_grupo_cliente +'</td>';
                            html_table += '<td>' + cliente.nombre +'</td>';
                            html_table += '<th scope="col"><button type="button" class="btn btn-warning" onclick="getGrupoId(' + cliente.id_grupo_cliente +')"><i class="far fa-edit"></i></button> <button type="button" class="btn btn-danger" onclick="deleteGrupo(' + cliente.id_grupo_cliente +')"><i class="far fa-trash-alt"></i></button></th>';
                            html_table += '</tr>';
                        }
                    }else{
                        html_table += '<tr>';
                        html_table += '<th colspan="6">No hay resultados</th>';
                        html_table += '</tr>';
                    }

                    html_table += '</tbody>';

                    html_table += '</table>';

                    $('#listado_clientes').html(html_table);

                }, error: function (xhr, textStatus, errorThrown) {
                    if(filtrar){
                        $('#buscar_clientes').text('Buscar');
                    }
                        console.log("cx ajax post error:" + xhr.statusText);
                    }
            });
        }

        function guardarForm(update = false){
            var action = 'saveGrupo';
            if(update){
                action = 'updateGrupo';
            }

            if(validaForm()){

                if(update)
                    $("#editar_form").text('Editando...');
                else
                    $("#guardar_form").text('Guardando...');

                setTimeout(() => {
                    $.ajax({
                    type: "POST",
                    url: "../controllers/grupo_clientes.php?action=" + action,
                    data: {
                        id_grupo_cliente: $("#id_grupo_cliente").val(),
                        nombre: $("#nombre").val(),
                    } ,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    success: function (res) {
                        if(update)
                            $("#editar_form").text('Editar');
                        else
                            $("#guardar_form").text('Guardar');

                        if(res.status){
                            $("#guardar_form").css('display', 'initial');
                            $("#editar_form").css('display', 'none');
                            msjTemporal(res.msj , 'success');
                            cancelarForm();
                        }else{
                            msjTemporal(res.msj , 'danger');
                        }

                    }, error: function (xhr, textStatus, errorThrown) {
                        $("#guardar_form").text('Guardar');
                        $("#editar_form").text('Editar');
                            console.log("cx ajax post error:" + xhr.statusText);
                        }
                    });
                }, 1500);
            }
        }

        function deleteGrupo(id_grupo_cliente){


            if(confirm('Se procederá a eliminar el grupo, hacé click en Ok para continuar.')){

                $.ajax({
                type: "POST",
                url: "../controllers/grupo_clientes.php?action=deleteGrupo",
                data: {
                    id_grupo_cliente: id_grupo_cliente
                } ,
                contentType: "application/x-www-form-urlencoded",
                dataType: "json",
                success: function (res) {

                    if(res.status){
                        msjTemporal(res.msj , 'success');
                        getGrupoClientes();
                    }else{
                        msjTemporal(res.msj , 'danger');
                    }

                }, error: function (xhr, textStatus, errorThrown) {

                        console.log("cx ajax post error:" + xhr.statusText);
                    }
                });

            }
        }

        function getGrupoId(id_grupo_cliente){

            $.ajax({
            type: "POST",
            url: "../controllers/grupo_clientes.php?action=getGrupoPorId",
            data: {
                id_grupo_cliente: id_grupo_cliente,
            } ,
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            success: function (res) {
                if(res.status){
                    $("#guardar_form").css('display', 'none');
                    $("#editar_form").css('display', 'initial');
                    $("#id_grupo_cliente").val(res.data.id_grupo_cliente);
                    $("#nombre").val(res.data.nombre);
                }else{
                    msjTemporal(res.msj , 'danger');
                }

            }, error: function (xhr, textStatus, errorThrown) {
                $("#guardar_form").text('Guardar');
                    console.log("cx ajax post error:" + xhr.statusText);
                }
            });

        }

        function validaForm(){

            if($("#nombre").val() == ""){
                msjTemporal("Ingresá el nombre del grupo." , 'danger');
                $("#nombre").focus();
                return false;
            }

            return true;
        }

        function msjTemporal(msj , clase){
            html = '<div class="alert alert-'+clase+'" role="alert">';
            html += msj;
            html += '</div>';

            $('#msj_alerta').html(html);
            setTimeout(() => {
                $('#msj_alerta').empty();
            }, 3000);
        }

        function cancelarForm(){
            $("#nombre").val('');
            getGrupoClientes();
        }


    </script>

<?php
include_once('../footer.php');
?>