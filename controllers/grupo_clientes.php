<?php

require_once "../models/grupo_clientes_model.php";
$grupo = new GrupoCliente();

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'getGrupos') {

    echo json_encode($grupo->getGrupos());

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'saveGrupo') {

    $data = array(
        "nombre" => $_POST['nombre'],
    );

    if ($grupo->saveGrupo($data)) {
        echo json_encode(array("status" => true, "msj" => "Grupo registrado."));
    } else {
        echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
    }

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'updateGrupo') {

    $data = array(
        "nombre" => $_POST['nombre'],
    );

    if ($grupo->updateGrupo($data, $_POST['id_grupo_cliente'])) {
        echo json_encode(array("status" => true, "msj" => "Datos actualizados."));
    } else {
        echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
    }

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'deleteGrupo') {

    if ($grupo->checkUso($_POST['id_grupo_cliente'])) {
        echo json_encode(array("status" => false, "msj" => "El grupo no se puede eliminar, actualmente esta vinculado con algunos clientes."));
    } else {
        if ($grupo->deleteGrupo($_POST['id_grupo_cliente'])) {
            echo json_encode(array("status" => true, "msj" => "Grupo eliminado"));
        } else {
            echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
        }
    }

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'buscarGrupo') {

    echo json_encode($grupo->buscarGrupo($_POST['text_buscar']));

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'getGrupoPorId') {

    if (!is_numeric($_REQUEST['id_grupo_cliente'])) {
        echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
    } else {
        echo json_encode(array("status" => true, "data" => $grupo->getGrupoPorId($_REQUEST['id_grupo_cliente'])[0]));
    }
}
