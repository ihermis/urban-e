<?php

require_once "../models/clientes_model.php";
$cliente = new Cliente();

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'getClientes') {

    

    echo json_encode($cliente->getClientes());

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'saveCliente') {

    if ($cliente->checkEmail($_POST['email'])) {
        echo json_encode(array("status" => false, "msj" => "Email registrado, consulta en el listado al cliente."));
    } else {
        $data = array(
            "nombre" => $_POST['nombre'],
            "apellido" => $_POST['apellido'],
            "email" => $_POST['email'],
            "grupo_cliente" => $_POST['grupo_cliente'],
            "observacion" => $_POST['observacion'],
        );

        if ($cliente->saveCliente($data)) {
            echo json_encode(array("status" => true, "msj" => "Cliente registrado."));
        } else {
            echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
        }
    }
}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'updateCliente') {

    $data = array(
        "nombre" => $_POST['nombre'],
        "apellido" => $_POST['apellido'],
        "email" => $_POST['email'],
        "grupo_cliente" => $_POST['grupo_cliente'],
        "observacion" => $_POST['observacion'],
    );

    if ($cliente->updateCliente($data, $_POST['id_cliente'])) {
        echo json_encode(array("status" => true, "msj" => "Datos actualizados."));
    } else {
        echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
    }

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'deleteCliente') {

    if ($cliente->deleteCliente($_POST['id_cliente'])) {
        echo json_encode(array("status" => true, "msj" => "Cliente eliminado"));
    } else {
        echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
    }

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'cambiarGrupoCliente') {


    if ($cliente->cambiarGrupoCliente($_POST['id_cliente'], $_POST['id_grupo_cliente'])) {
        echo json_encode(array("status" => true, "msj" => "Grupo de cliente cambiado"));
    } else {
        echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
    }

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'buscarClientes') {

    echo json_encode($cliente->buscarClientes($_POST['text_buscar'], $_POST['grupo_cliente_buscar']));

}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'getClientePorId') {

    if (!is_numeric($_REQUEST['id_cliente'])) {
        echo json_encode(array("status" => false, "msj" => "Ocurrió un error, vuelve a intentarlo."));
    } else {
        echo json_encode(array("status" => true, "data" => $cliente->getClienteporId($_REQUEST['id_cliente'])[0]));
    }
}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'getGrupoClientes') {

    echo json_encode($cliente->getGrupoClientes());

}
