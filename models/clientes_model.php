<?php
require_once '../config/conexion_db.php';

class Cliente
{

    private $cliente;
    private $grupoCliente;
    private $conectar;
    private $db;

    public function __construct()
    {
        $this->grupoCliente = array();
        $this->cliente = array();
        $this->conectar = new Conectar();
        $this->db = $this->conectar->conexion();
    }

    public function getClientes()
    {
        $sql = "SELECT c.id id_cliente , c.nombre, c.apellido, c.email, c.grupo_cliente_id, c.observacion , gc.nombre grupo_cliente
            FROM cliente c
            JOIN grupo_cliente gc ON gc.id = c.grupo_cliente_id
            ";
        foreach ($this->db->query($sql) as $res) {
            $this->cliente[] = $res;
        }
        return $this->cliente;
    }

    public function buscarClientes($text_buscar, $grupo_cliente)
    {

        $where = '';

        if ($text_buscar != '') {
            $where_t = " c.nombre like '%" . $text_buscar . "%' OR c.apellido like '%" . $text_buscar . "%' OR c.email like '%" . $text_buscar . "%' ";
        }

        if ($grupo_cliente != '' && is_numeric($grupo_cliente)) {
            $where_n = " c.grupo_cliente_id=" . $grupo_cliente . " ";
        }

        if ($text_buscar != '' && $grupo_cliente != '' && is_numeric($grupo_cliente)) {
            $where = 'WHERE ' . $where_t . ' AND ' . $where_n;
        } elseif ($text_buscar != '' && $grupo_cliente == '') {
            $where = 'WHERE ' . $where_t . ' ';
        } elseif ($text_buscar == '' && $grupo_cliente != '' && is_numeric($grupo_cliente)) {
            $where = 'WHERE ' . $where_n . ' ';
        }

        $sql = "SELECT c.id id_cliente , c.nombre, c.apellido, c.email, c.grupo_cliente_id, c.observacion , gc.nombre grupo_cliente
            FROM cliente c
            JOIN grupo_cliente gc ON gc.id = c.grupo_cliente_id
            $where
            GROUP BY c.id
            ";

        foreach ($this->db->query($sql) as $res) {
            $this->cliente[] = $res;
        }
        return $this->cliente;
    }

    public function saveCliente($data)
    {

        $sql = "INSERT INTO cliente (`nombre`, `apellido`, `email`, `grupo_cliente_id`, `observacion`)
        VALUES (
                '" . $data['nombre'] . "',
                '" . $data['apellido'] . "',
                '" . $data['email'] . "',
                " . $data['grupo_cliente'] . ",
                '" . $data['observacion'] . "');";

        $result = $this->db->query($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function updateCliente($data, $id_cliente)
    {

        $sql = "UPDATE cliente SET
            nombre='" . $data['nombre'] . "',
            apellido='" . $data['apellido'] . "',
            email='" . $data['email'] . "',
            grupo_cliente_id=" . $data['grupo_cliente'] . ",
            observacion='" . $data['observacion'] . "'
            WHERE id=" . $id_cliente;

        $result = $this->db->query($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function cambiarGrupoCliente($id_cliente, $id_grupo_cliente)
    {

        $sql = "UPDATE cliente SET
            grupo_cliente_id=" . $id_grupo_cliente . "
            WHERE id=" . $id_cliente;

        $result = $this->db->query($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteCliente($id_cliente)
    {

        $sql = "DELETE FROM cliente WHERE id=" . $id_cliente;

        $result = $this->db->query($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function getGrupoClientes()
    {

        $sql = "SELECT gc.nombre , gc.id id_grupo_cliente FROM grupo_cliente gc";
        foreach ($this->db->query($sql) as $res) {
            $this->grupoCliente[] = $res;
        }
        return $this->grupoCliente;
    }

    public function getClienteporId($id)
    {

        $sql = "SELECT c.id id_cliente , c.nombre, c.apellido, c.email, c.grupo_cliente_id, c.observacion , gc.nombre grupo_cliente
            FROM cliente c
            JOIN grupo_cliente gc ON gc.id = c.grupo_cliente_id
            WHERE c.id=" . $id;

        foreach ($this->db->query($sql) as $res) {
            $this->cliente[] = $res;
        }
        return $this->cliente;
    }

    public function checkEmail($email)
    {

        $sql = "SELECT c.* FROM cliente c WHERE c.email='$email'";
        $result = $this->db->query($sql);

        if ($result->num_rows) {
            return true;
        } else {
            return false;
        }
    }
}
