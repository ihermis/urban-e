<?php
require_once '../config/conexion_db.php';

class GrupoCliente
{

    private $grupo;
    private $conectar;
    private $db;

    public function __construct()
    {
        $this->grupo = array();
        $this->conectar = new Conectar();
        $this->db = $this->conectar->conexion();
    }

    public function getGrupos()
    {
        $sql = "SELECT gc.id id_grupo_cliente , gc.nombre
            FROM grupo_cliente gc
            ";
        foreach ($this->db->query($sql) as $res) {
            $this->grupo[] = $res;
        }
        return $this->grupo;
    }

    public function buscarGrupo($text_buscar)
    {

        $where = '';

        if ($text_buscar != '') {
            $where = " WHERE gc.nombre like '%" . $text_buscar . "%' ";
        }

        $sql = "SELECT gc.id id_grupo_cliente , gc.nombre
            FROM grupo_cliente gc
            $where
            GROUP BY gc.id
            ";

        foreach ($this->db->query($sql) as $res) {
            $this->grupo[] = $res;
        }
        return $this->grupo;
    }

    public function saveGrupo($data)
    {

        $sql = "INSERT INTO grupo_cliente (`nombre`)  VALUES ('" . $data['nombre'] . "');";

        $result = $this->db->query($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function updateGrupo($data, $id_cliente)
    {

        $sql = "UPDATE grupo_cliente SET
            nombre='" . $data['nombre'] . "'
            WHERE id=" . $id_cliente;

        $result = $this->db->query($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteGrupo($id_cliente)
    {

        $sql = "DELETE FROM grupo_cliente WHERE id=" . $id_cliente;

        $result = $this->db->query($sql);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }


    public function getGrupoPorId($id)
    {

        $sql = "SELECT gc.id id_grupo_cliente , gc.nombre
            FROM grupo_cliente gc           
            WHERE gc.id=" . $id;

        foreach ($this->db->query($sql) as $res) {
            $this->grupo[] = $res;
        }
        return $this->grupo;
    }

    public function checkUso($id_grupo_cliente)
    {

        $sql = "SELECT c.* FROM cliente c WHERE c.grupo_cliente_id=".$id_grupo_cliente;
        $result = $this->db->query($sql);

        if ($result->num_rows) {
            return true;
        } else {
            return false;
        }
    }
}
